#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# PyParadox is a nix launcher for Paradox titles.
# Copyright (C) 2014  Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import sys

def main():
    try:
        file_ = open("coverage.xml")
    except FileNotFoundError as e:
        print("Could not find coverage.xml.")
        print("Aborting.")
        sys.exit()

    lines = file_.read()
    file_.close()

    matches = re.findall(r'(?<=line-rate=")\d\.\d+', lines)

    # The first match is the project-wide match.
    match = matches[0]
    match = float(match) * 100

    print("{0:.2f}% covered".format(match))


if __name__ == "__main__":
    main()

