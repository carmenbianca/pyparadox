# -*- coding: utf-8 -*-

import logging


# Disable these messages during testing so they don't clutter the results.
# Re-enable them if they add any value.
logging.disable(logging.CRITICAL)
